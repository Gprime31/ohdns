# OhDNS

## Warning
Don't use this to do dark things.

## How it works
A wrapper does subdomain enumeration with:
* Subfinder
* Amass
* Massdns
* Gotator - Subdomain permutation
* GoWC - Wildcard cleaner. From [GoWC](https://github.com/sting8k/GoWC)

## Installation

```
git clone https://gitlab.com/prawps/ohdns
```

## Usage

* (Should have) Amass config file. `-ac`
* (Should have) Subfinder config file. `-sc`

```
./ohdns.sh --help


 ██████╗ ██╗  ██╗██████╗ ███╗   ██╗███████╗
██╔═══██╗██║  ██║██╔══██╗████╗  ██║██╔════╝
██║   ██║███████║██║  ██║██╔██╗ ██║███████╗
██║   ██║██╔══██║██║  ██║██║╚██╗██║╚════██║
╚██████╔╝██║  ██║██████╔╝██║ ╚████║███████║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                OhDNS v1.4

Very fast & accurate dns resolving and bruteforcing.

Usage:

        Example:
                ohdns [args] [--skip-wildcard-check] [--help] -wl wordlists/small_wordlist.txt -d domain.com
                ohdns -wl wordlists/small_wordlist.txt -d domain.com -w output.txt -gt

        Optional:

                -d, --domain <domain>   Target to scan
                -wl, --wordlist <filename>      Wordlist to do bruteforce
                -sc, --subfinder-config <filename>      SubFinder config file
                -ac, --amass-config     <filename>      Amass config file
                -i, --ips       Show ips in output
                -gt, --gotator  Use gotator to create & bruteforce permutation list
                -mg, --max-gotator	Max generated gotator permutations list (Default: 10mil lines)
                -sw, --skip-wildcard-check              Do no perform wildcard detection and filtering

                -w,  --write <filename>                 Write valid domains to a file

                -h, --help                              Display this message
```

## Example
```
./ohdns.sh -wl wordlists/small_wordlist.txt -d example.com -w example.txt -i -gt


 ██████╗ ██╗  ██╗██████╗ ███╗   ██╗███████╗
██╔═══██╗██║  ██║██╔══██╗████╗  ██║██╔════╝
██║   ██║███████║██║  ██║██╔██╗ ██║███████╗
██║   ██║██╔══██║██║  ██║██║╚██╗██║╚════██║
╚██████╔╝██║  ██║██████╔╝██║ ╚████║███████║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                ohdns v1.4

Very fast & accurate dns resolving and bruteforcing.

[!] Tempdir: /tmp/ohdns.qU256fRf
[*] [SubFinder] Running ...
[!] [SubFinder] Finished | Duration: 6s | Subdomains: 1732
[*] [Amass] Running ...
[!] [Amass] Finished | Duration: 122s | Subdomains: 3223
[*] Merging wordlist ...
[*] Preparing list of domains for massdns...
[*] Sanitizing list...
[!] 138872 domains to resolve with massdns
[*] [MassDNS] Invoking massdns... this can take some time
[*] [MassDNS] Running the 1st time ...
[!] [MassDNS] Finished | Duration: 23s
[*] [MassDNS] Running the 2nd time ...
[!] [MassDNS] Finished | Duration: 29s
[*] [MassDNS] Merging output from 2 times.
[!] [MassDNS] 360 domains returned a DNS answer
[*] [GoWC] Cleaning wildcard root subdomains...
[!] [GoWC] Finished | Duration: 6s
[!] Found 360 valid domains!
[*] Exporting domains to /tmp/ohdns.qU256fRf/phase1.txt

[*] [Gotator] Permutating subdomains ...
[!] [Gotator] Finished | Made more 2695935 subdomains to resolve
[*] [MassDNS] Invoking massdns... this can take some time
[*] [MassDNS] Running the 1st time ...
[!] [MassDNS] Finished | Duration: 220s
[*] [MassDNS] Running the 2nd time ...
[!] [MassDNS] Finished | Duration: 246s
[*] [MassDNS] Merging output from 2 times.
[!] [MassDNS] 403 domains returned a DNS answer
[*] [GoWC] Cleaning wildcard root subdomains...
[!] [GoWC] Finished | Duration: 3s
[!] Found 403 domains from permutation!
[*] Exporting domains to /tmp/ohdns.qU256fRf/phase2.txt

[!] Total: 763 valid domains in 0h11m52s.
[!] Output: example.txt

```

```
cat output.txt
...
partner-service.<example.com> 	 [x.x.65.172]
partner-service-testing.<example.com> 	 [x.x.65.172]
partner.<example.com> 	 [edge-web.dual-gslb.<example.com>.]
partners.<example.com> 	 [edge-web.dual-gslb.<example.com>.]
partners.wg.<example.com> 	 [edge-web.dual-gslb.<example.com>.]
payment-callback.<example.com> 	 [x.x.65.172]
pci.<example.com> 	 [x.x.36.21 x.x.34.21 x.x.32.21 x.x.38.21]
pci-testing.<example.com> 	 [x.x.36.21 x.x.34.21 x.x.38.21 x.x.32.21]
...

```

